﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Utility
{
  public static class MathBetter
  {
    /// <summary>
    /// Modulo = A mod B which is always in &lt;0, B) range.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static float Mod(float a, float b)
    {
      return (a % b + b) % b;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="current"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public static float AngleDelta(float current, float target)
    {
      float num = Mod(target - current, 360f);
      if (num > 180f)
        num = 360f - num;

      return num;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="current"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public static float AngleSignedDelta(float current, float target)
    {
      float diff = target - current;
      float num = Mod(diff, 360f);
      if (num > 180f)
        num = num - 360f;

      return num;
    }
  }
}
