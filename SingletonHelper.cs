﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Runaurufu.Utility
{
  public static class SingletonHelper
  {
    public static T GetSingleInstance<T>()
    {
      return GetInstances<T>().FirstOrDefault();
    }

    public static IEnumerable<T> GetInstances<T>()
    {
      Type t = typeof(T);
      foreach (Assembly ass in AppDomain.CurrentDomain.GetAssemblies())
      {
        Type[] types = null;
        try
        {
          types = ass.GetTypes();
        }
        catch (Exception)
        {
        }

        if (types != null)
        {
          foreach (Type type in types)
          {
            if (type.IsInterface)
              continue;

            if (t.IsAssignableFrom(type))
              yield return GetInstanceFromType<T>(type);
          }
        }
      }
    }

    private static T GetInstanceFromType<T>(Type type)
    {
      MethodInfo info = type.GetMethod("GetInstance", BindingFlags.Public | BindingFlags.Static);

      if (info != null)
      {
        PropertyInfo prop = type.GetProperty("Instance", BindingFlags.Static | BindingFlags.Public);
        if (prop != null)
          info = prop.GetGetMethod();
      }

      if (info != null)
        return (T)info.Invoke(null, null);

      FieldInfo fieldInfo = type.GetField("instance", BindingFlags.Public | BindingFlags.Static);
      if (fieldInfo != null)
        return (T)fieldInfo.GetValue(null);

      return default(T);
    }
  }
}
