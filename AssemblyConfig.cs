﻿using ICities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Runaurufu.Utility
{
  public sealed class AssemblyConfig
  {
    //public static readonly string ConfigName = Assembly.GetExecutingAssembly().GetName().Name;
    //public static readonly string ConfigFileName = ConfigName + ".config";

    public static readonly string GameDirectoryPath = Path.GetFullPath(Environment.CurrentDirectory);
    //internal static readonly string AssemblyDirectoryPath = Path.GetFullPath(Assembly.GetExecutingAssembly().Location);

    public static string GetConfigName(Assembly ass)
    {
      return ass.GetName().Name;
    }

    public static string GetConfigFileName(Assembly ass)
    {
      return GetConfigName(ass) + ".config";
    }

    public static string GetConfigFileName()
    {
      Assembly callingAssembly = Assembly.GetCallingAssembly();
      return GetConfigName(callingAssembly) + ".config";
    }
  }
}
