﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Runaurufu.Utility
{
  public static class ExtensionHelper
  {
    public static object InvokeMethod(this object obj, string name, params object[] parameters)
    {
      Type t = obj.GetType();
      MethodInfo method = t.GetMethod(name, BindingFlags.Instance | BindingFlags.NonPublic);
      if (method == null)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, string.Format("Invoke failed! (method {0} not found in type {1})", name, t.Name));
        return null;
      }

      try
      {
        return method.Invoke(obj, parameters);
      }
      catch (Exception ex)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, string.Format("Invoke failed (method {0} in type {1}) with message: {2}", name, t.Name, ex.Message));
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, ex.ToString());
        return null;
      }
    }

    public static void SetFieldValue(this object obj, string name, object value)
    {
      Type t = obj.GetType();
      FieldInfo field = t.GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);

      if (field == null)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, string.Format("SetFieldValue failed! (field {0} not found in type {1})", name, t.Name));
        return;
      }

      try
      {
        field.SetValue(obj, value);
      }
      catch (Exception ex)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, string.Format("SetFieldValue failed (field {0} in type {1}) with message: {2} ", name, t.Name, ex.Message));
        return;
      }
    }

    public static object GetFieldValue(this object obj, string name)
    {
      Type t = obj.GetType();
      FieldInfo field = t.GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);

      if (field == null)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, string.Format("GetFieldValue failed! (field {0} not found in type {1})", name, t.Name));
        return null;
      }

      try
      {
        return field.GetValue(obj);
      }
      catch (Exception ex)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, string.Format("GetFieldValue failed (field {0} in type {1}) with message {2}", name, t.Name, ex.Message));
        return null;
      }
    }

    public static int GetIndexOf(this Array obj, object searchedItem)
    {
      for (int i = 0; i < obj.Length; i++)
      {
        if (object.ReferenceEquals(obj.GetValue(i), searchedItem))
          return i;
      }
      return -1;
    }
  }

  internal static class TextureHelper
  {
    public static bool SaveToFile(Texture2D texture, string filePath)
    {
      try
      {
        byte[] data = texture.GetRawTextureData();
        byte[] dataArray = new byte[data.Length + 128];
        Buffer.BlockCopy(data, 0, dataArray, 128, data.Length);

        byte[] aux = BitConverter.GetBytes(texture.width);
        Buffer.BlockCopy(aux, 0, dataArray, 16, aux.Length);
        aux = BitConverter.GetBytes(texture.height);
        Buffer.BlockCopy(aux, 0, dataArray, 12, aux.Length);

        File.WriteAllBytes(filePath, dataArray);
        return true;
      }
      catch (Exception ex)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, ex.ToString());
        return false;
      }
    }

    public static Texture2D LoadFromFile(string filePath)
    {
      Texture2D texture = null;
      try
      {
        byte[] array = File.ReadAllBytes(filePath);
        int width = BitConverter.ToInt32(array, 16);
        int height = BitConverter.ToInt32(array, 12);
        texture = new Texture2D(width, height, TextureFormat.DXT5, true);
        //List<byte> list = new List<byte>();
        //for (int i = 0; i < array.Length; i++)
        //{
        //  bool flag2 = i > 127;
        //  if (flag2)
        //  {
        //    list.Add(array[i]);
        //  }
        //}

        byte[] dataArray = new byte[array.Length - 128];
        Buffer.BlockCopy(array, 128, dataArray, 0, dataArray.Length);

        texture.LoadRawTextureData(dataArray);
        texture.name = Path.GetFileName(filePath);
        texture.anisoLevel = 8;
        texture.Apply();
      }
      catch (Exception ex)
      {
        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, ex.ToString());
      }
      return texture;
    }
  }

  public class TextureStorage
  {
    public Dictionary<string, Texture2D> Textures;

    public TextureStorage()
    {
      this.Textures = new Dictionary<string, Texture2D>();
    }

    public void Add(string key, Texture2D texture)
    {
      if (string.IsNullOrEmpty(key) || texture == null)
        return;

      if (this.Textures.ContainsKey(key))
        return;

      this.Textures.Add(key, texture);
      DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "Added " + key);
    }

    public Texture2D Get(string key)
    {
      if (this.Textures.ContainsKey(key) == false)
        return null;

      return this.Textures[key];
    }

    public void AddFromMaterial(string key, Material material, string textureName)
    {
      Texture2D texture2D = material.GetTexture(textureName) as Texture2D;
      if (texture2D != null)
        this.Add(key, texture2D);
    }

    public void SetToMaterial(string key, Material material, string textureName)
    {
      Texture2D texture = this.Get(key);
      if (texture == null)
        return;

      material.SetTexture(textureName, texture);
    }

    public void ClearTextures(bool dispose)
    {
      if (this.Textures == null)
        return;

      if (dispose)
      {
        foreach (KeyValuePair<string, Texture2D> pair in this.Textures)
        {
          UnityEngine.Object.DestroyImmediate(pair.Value);
        }
      }
    }

    public void SaveToStream(Stream stream)
    {
      byte[] buffer, buffer2;

      buffer = BitConverter.GetBytes(this.Textures.Count);
      stream.Write(buffer, 0, buffer.Length);

      foreach (KeyValuePair<string, Texture2D> pair in this.Textures)
      {
        buffer = Encoding.UTF32.GetBytes(pair.Key);
        buffer2 = BitConverter.GetBytes(buffer.Length);
        stream.Write(buffer2, 0, buffer2.Length);
        stream.Write(buffer, 0, buffer.Length);

        buffer = BitConverter.GetBytes(pair.Value.width);
        stream.Write(buffer, 0, buffer.Length);

        buffer = BitConverter.GetBytes(pair.Value.height);
        stream.Write(buffer, 0, buffer.Length);

        buffer = pair.Value.GetRawTextureData();
        buffer2 = BitConverter.GetBytes(buffer.Length);
        stream.Write(buffer2, 0, buffer2.Length);
        stream.Write(buffer, 0, buffer.Length);
      }
    }

    public void LoadFromStream(Stream stream)
    {
      byte[] intBuffer, buffer;

      intBuffer = new byte[4];
      stream.Read(intBuffer, 0, 4);

      long items = BitConverter.ToInt32(intBuffer, 0);
      for (int i = 0; i < items; i++)
      {
        stream.Read(intBuffer, 0, 4);
        int nameLength = BitConverter.ToInt32(intBuffer, 0);

        buffer = new byte[nameLength];
        stream.Read(buffer, 0, nameLength);
        string name = Encoding.UTF32.GetString(buffer, 0, nameLength);

        stream.Read(intBuffer, 0, 4);
        int width = BitConverter.ToInt32(intBuffer, 0);

        stream.Read(intBuffer, 0, 4);
        int height = BitConverter.ToInt32(intBuffer, 0);

        stream.Read(intBuffer, 0, 4);
        int dataLength = BitConverter.ToInt32(intBuffer, 0);

        buffer = new byte[dataLength];
        stream.Read(buffer, 0, dataLength);

        try
        {
          Texture2D texture = new Texture2D(width, height, TextureFormat.DXT5, true);
          texture.LoadRawTextureData(buffer);
          texture.name = name;
          texture.anisoLevel = 8;
          texture.Apply();

          this.Textures.Add(name, texture);
        }
        catch (Exception ex)
        {
          DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, ex.ToString());
        }
      }
    }
  }
}