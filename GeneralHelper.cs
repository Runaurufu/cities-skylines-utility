﻿using ColossalFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Runaurufu.Utility
{
  public static class GeneralHelper
  {
    /// <summary>
    /// Retrieve terrain world position from current mouse position.
    /// </summary>
    /// <param name="terrainPosition"></param>
    /// <returns></returns>
    public static bool GetTerrainPositionFromMouse(out Vector3 terrainPosition)
    {
      Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
      ToolBase.RaycastInput input = new ToolBase.RaycastInput(mouseRay, Camera.main.farClipPlane);

      Vector3 origin = input.m_ray.origin;
      Vector3 normalized = input.m_ray.direction.normalized;
      Vector3 vector = input.m_ray.origin + normalized * input.m_length;
      ColossalFramework.Math.Segment3 ray = new ColossalFramework.Math.Segment3(origin, vector);

      if (Singleton<TerrainManager>.instance.RayCast(ray, out terrainPosition) && !input.m_ignoreTerrain)
      {
        float num2 = Vector3.Distance(terrainPosition, origin) + 100f;
        if (num2 < input.m_length)
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Moves camera to specified position.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public static bool MoveCameraToPosition(Vector3 position)
    {
      GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
      if (gameObject != null)
      {
        CameraController camera = gameObject.GetComponent<CameraController>();
        camera.m_targetPosition = position;
        return true;
      }
      return false;
    }

    //public static WaterManager.Cell GetWaterCell(Vector3 position)
    //{
    //  WaterManager.Cell[] waterGrid = (WaterManager.Cell[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterGrid");

    //  int x = Mathf.Clamp((int)(position.x / 38.25f + 128f), 0, 255);
    //  int z = Mathf.Clamp((int)(position.z / 38.25f + 128f), 0, 255);

    //  //waterGrid[z * 256 + x].
    //}

    /// <summary>
    /// Retrieve terrain information at given position.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="terrainHeight"></param>
    /// <param name="waterHeight"></param>
    /// <param name="pollutionHeight"></param>
    public static void GetTerrainInformation(Vector3 position, out ushort terrainHeight, out ushort waterHeight, out ushort pollutionHeight)
    {
      uint waterFrameIndex = (uint)TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterFrameIndex");
      ushort[] heightBuffer = (ushort[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_heightBuffer");
      WaterSimulation.Cell[][] waterBuffers = (WaterSimulation.Cell[][])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterBuffers"); ;

      GetTerrainInformationImpl(waterFrameIndex, ref heightBuffer, ref waterBuffers, position, out terrainHeight, out waterHeight, out pollutionHeight);

    }

    private static void GetTerrainInformationImpl(uint waterFrameIndex, ref ushort[] heightBuffer, ref WaterSimulation.Cell[][] waterBuffers, Vector3 position, out ushort terrainHeight, out ushort waterHeight, out ushort pollutionHeight)
    {
      waterHeight = 0;
      pollutionHeight = 0;
      terrainHeight = 0;

      float num3 = 16f;
      int num4 = 1080;
      float num6 = (float)num4 * num3;

      int num92 = Mathf.CeilToInt((position.x + num6 * 0.5f) / num3);
      int num93 = Mathf.CeilToInt((position.z + num6 * 0.5f) / num3);

      int num101 = num93 * (num4 + 1) + num92;
      if (num101 < 0 || num101 >= heightBuffer.Length)
        return;

      // Get Terrain Height
      terrainHeight = heightBuffer[num101];

      // Get Water Height
      uint num7 = waterFrameIndex & 4294967232u;
      int waterBuffersIndex = (int)((UIntPtr)(num7 >> 6 & 1u));

      if (waterBuffersIndex < 0 || waterBuffersIndex >= waterBuffers.Length)
        return;

      WaterSimulation.Cell[] array5 = waterBuffers[waterBuffersIndex];

      if (num101 < 0 || num101 >= array5.Length)
        return;

      WaterSimulation.Cell cell13 = array5[num101];

      waterHeight = cell13.m_height;
      pollutionHeight = cell13.m_pollution;
      
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="resolution">Every how many game units measurement should be done.</param>
    /// <param name="averageTerrainHeight"></param>
    /// <param name="averageWaterHeight"></param>
    /// <param name="averagePollutionHeight"></param>
    public static void GetMapAverages(float resolution, out float averageTerrainHeight, out float averageWaterHeight, out float averagePollutionHeight)
    {
      uint waterFrameIndex = (uint)TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterFrameIndex");
      ushort[] heightBuffer = (ushort[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_heightBuffer");
      WaterSimulation.Cell[][] waterBuffers = (WaterSimulation.Cell[][])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterBuffers"); ;

      averageTerrainHeight = 0;
      averageWaterHeight = 0;
      averagePollutionHeight = 0;

      int probes = 0;
      for (float x = -8640; x <= 8640; x += resolution)
      {
        for (float z = -8640; z <= 8640; z += resolution)
        {
          Vector3 position = new Vector3(x, 0, z);

          ushort terrainHeight, waterHeight, pollutionHeight;

          GetTerrainInformationImpl(waterFrameIndex, ref heightBuffer, ref waterBuffers, position, out terrainHeight, out waterHeight, out pollutionHeight);

          averageTerrainHeight += terrainHeight;
          averageWaterHeight += waterHeight;
          averagePollutionHeight += pollutionHeight;
          probes++;
        }
      }

      averageTerrainHeight /= probes;
      averageWaterHeight /= probes;
      averagePollutionHeight /= probes;
    }

    /// <summary>
    /// Tries to add specified amount of water at specified position. Returns amount of water which was actually added.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="waterToAdd"></param>
    /// <returns></returns>
    public static ushort TryAddWater(Vector3 position, ushort waterToAdd)
    {
      uint waterFrameIndex = (uint)TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterFrameIndex");
      ushort[] heightBuffer = (ushort[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_heightBuffer");
      WaterSimulation.Cell[][] waterBuffers = (WaterSimulation.Cell[][])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterBuffers"); ;

      ushort waterHeight, pollutionHeight, terrainHeight;

      GetTerrainInformationImpl(waterFrameIndex, ref heightBuffer, ref waterBuffers, position, out terrainHeight, out waterHeight, out pollutionHeight);

      ushort freeSpace = (ushort)Math.Max(ushort.MaxValue - terrainHeight - waterHeight - pollutionHeight, 0);

      if (waterToAdd > freeSpace)
        waterToAdd = freeSpace;

      if (waterToAdd > 0)
      {
        int bitCells = (int)TerrainManager.instance.WaterSimulation.GetFieldValue("m_bitCells");

        float num3 = 16f;
        int num4 = 1080;
        float num6 = (float)num4 * num3;
        uint num7 = waterFrameIndex & 4294967232u;

        uint num12 = (num7 >> 6) % 3u;
        ulong[] array2;
        if (num12 == 0u)
        {
          array2 = (ulong[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterExists1");
        }
        else if (num12 == 1u)
        {
          array2 = (ulong[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterExists2");
        }
        else
        {
          array2 = (ulong[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterExists3");
        }

        WaterSimulation.Cell[] array5 = waterBuffers[(int)((UIntPtr)(num7 >> 6 & 1u))];

        int num92 = Mathf.CeilToInt((position.x + num6 * 0.5f) / num3);
        int num93 = Mathf.CeilToInt((position.z + num6 * 0.5f) / num3);

        int num101 = num93 * (num4 + 1) + num92;

        array5[num101].m_height += waterToAdd;

        if (array5[num101].m_height != 0)
        {
          array2[num93 / bitCells] |= 1uL << num92 / bitCells;
        }
      }

      return waterToAdd;
    }

    /// <summary>
    /// Tries to get specified amount of water at specified position. Returns amount of water which was actually retrieved.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="waterToAdd"></param>
    /// <returns></returns>
    public static ushort TryGetWater(Vector3 position, ushort waterToGet)
    {
      uint waterFrameIndex = (uint)TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterFrameIndex");
      ushort[] heightBuffer = (ushort[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_heightBuffer");
      WaterSimulation.Cell[][] waterBuffers = (WaterSimulation.Cell[][])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterBuffers"); ;

      ushort waterHeight, pollutionHeight, terrainHeight;

      GetTerrainInformationImpl(waterFrameIndex, ref heightBuffer, ref waterBuffers, position, out terrainHeight, out waterHeight, out pollutionHeight);

      ushort freeSpace = (ushort)Math.Max(ushort.MaxValue - terrainHeight - waterHeight - pollutionHeight, 0);

      if (waterToGet > waterHeight)
        waterToGet = waterHeight;

      if (waterToGet > 0)
      {
        int bitCells = (int)TerrainManager.instance.WaterSimulation.GetFieldValue("m_bitCells");

        float num3 = 16f;
        int num4 = 1080;
        float num6 = (float)num4 * num3;
        uint num7 = waterFrameIndex & 4294967232u;

        uint num12 = (num7 >> 6) % 3u;
        ulong[] array2;
        if (num12 == 0u)
        {
          array2 = (ulong[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterExists1");
        }
        else if (num12 == 1u)
        {
          array2 = (ulong[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterExists2");
        }
        else
        {
          array2 = (ulong[])TerrainManager.instance.WaterSimulation.GetFieldValue("m_waterExists3");
        }

        WaterSimulation.Cell[] array5 = waterBuffers[(int)((UIntPtr)(num7 >> 6 & 1u))];

        int num92 = Mathf.CeilToInt((position.x + num6 * 0.5f) / num3);
        int num93 = Mathf.CeilToInt((position.z + num6 * 0.5f) / num3);

        int num101 = num93 * (num4 + 1) + num92;

        array5[num101].m_height -= waterToGet;

        if (array5[num101].m_height != 0)
        {
          array2[num93 / bitCells] |= 1uL << num92 / bitCells;
        }
      }

      return waterToGet;
    }
  }
}
