﻿using ColossalFramework.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Runaurufu.Utility
{
  public class UIPanelAdv : UIPanel
  {
    private bool pauseUpdates = false;
    private bool inDragMode = false;
    private Vector2 dragStartMousePosition;
    private Vector2 dragStartPanelPosition;

    /// <summary>
    /// StartPosition is set to center screen on UIPanelAdv.Awake() call.
    /// If you want to use your own position then change it after Awake() was called.
    /// </summary>
    public Vector3 StartPosition;

    public override void Awake()
    {
      base.Awake();

      float desiredTopPos = ((float)Screen.height - this.height) * 0.5f;
      float desiredLeftPos = ((float)Screen.width - this.width) * 0.5f;

      this.StartPosition.x = desiredLeftPos;
      this.StartPosition.y = desiredTopPos;
    }

    public override void Start()
    {
      base.Start();

      this.absolutePosition = this.StartPosition;
    }

    protected override void OnVisibilityChanged()
    {
      base.OnVisibilityChanged();
      //if(this.isVisible)
     //   this.absolutePosition = new Vector3(120f, 400f, 0.0f);
    }

    protected bool IsUpdateAllowed
    {
      get { return this.pauseUpdates == false && this.inDragMode == false && this.isVisible && this.isEnabled; }
    }

    protected override void OnMouseDown(UIMouseEventParameter p)
    {
      base.OnMouseDown(p);

      if (this.inDragMode == false)
      {
        this.dragStartMousePosition = p.position;
        this.dragStartPanelPosition = this.absolutePosition;
      }
    }

    protected override void OnMouseLeave(UIMouseEventParameter p)
    {
      base.OnMouseLeave(p);

      this.inDragMode = false;
    }

    protected override void OnMouseUp(UIMouseEventParameter p)
    {
      base.OnMouseUp(p);

      this.inDragMode = false;
    }

    protected override void OnDragStart(UIDragEventParameter p)
    {
      base.OnDragStart(p);

      this.inDragMode = true;
    }

    protected override void OnMouseMove(UIMouseEventParameter p)
    {
      base.OnMouseMove(p);

      if (this.inDragMode)
      {
        float dx = p.position.x - this.dragStartMousePosition.x;
        float dy = this.dragStartMousePosition.y - p.position.y;

        this.absolutePosition = new Vector3(this.dragStartPanelPosition.x + dx, this.dragStartPanelPosition.y + dy, 0.0f);
      }
    }

    protected override void OnDragEnd(UIDragEventParameter p)
    {
      base.OnDragEnd(p);

      this.inDragMode = false;
    }
  }
}
