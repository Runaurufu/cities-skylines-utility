﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Utility
{
  public static class PhysicsHelper
  {
    /// <summary>
    /// According to https://www.skylinescity.com/cities-skylines-faq each tile is 2km x 2km.
    /// </summary>
    public const float TileLengthMeters = 2000f;
    /// <summary>
    /// According to https://www.skylinescity.com/cities-skylines-faq each tile is 2km x 2km.
    /// </summary>
    public const float TileLengthGameUnits = 1920f;

    /// <summary>
    /// There are 81 tiles.
    /// </summary>
    /// <returns></returns>
    /// </summary>
    public const float MapSurfaceAreaMeters = 81f * TileLengthMeters * TileLengthMeters;
    /// <summary>
    /// There are 81 tiles.
    /// </summary>
    public const float MapSurfaceAreaGameUnits = 81f * TileLengthGameUnits * TileLengthGameUnits;



    /// <summary>
    /// 1x = 1y = how many meters?
    /// </summary>
    public const float UnitLengthMeters = TileLengthMeters / TileLengthGameUnits;

    /// <summary>
    /// 1z = how many meters?
    /// </summary>
    public const float UnitHeightMeters = 1.5f;
    /// <summary>
    /// how many 1z = how many x,y units?
    /// </summary>
    public const float UnitHeightLengthGameUnits = UnitHeightMeters / UnitLengthMeters;

    public const float PlaneFlyHeightGameUnits = 1495f;
    public const float PlaneFlyHeightMetres = PlaneFlyHeightGameUnits * UnitHeightMeters;

    public const float MaxTerrainHeightGameUnits = 1024f;
    public const float MaxTerrainHeightMeters = MaxTerrainHeightGameUnits * UnitHeightMeters;

    public const float MapMaxTerrainVolumeMeters = MapSurfaceAreaMeters * MaxTerrainHeightMeters;

    public const float CloudTopHeightGameUnits = 820f;
    public const float CloudTopHeightMeters = CloudTopHeightGameUnits * UnitHeightMeters;

    /// <summary>
    /// Number of water cells on the map.
    /// int num = 1080;
    /// this.m_waterBuffers = new WaterSimulation.Cell[2][];
    /// this.m_waterBuffers[i] = new WaterSimulation.Cell[(num + 1) * (num + 1)];
    /// </summary>
    public const int WaterCellsCount = 1081 * 1081 * 2;

    public const float WaterCellMaxHeightGameUnits = ushort.MaxValue / MaxTerrainHeightGameUnits;
    public const float WaterCellMaxHeightMeters = WaterCellMaxHeightGameUnits * UnitHeightMeters;

    public const float WaterCellSurfaceAreaGameUnits = MapSurfaceAreaGameUnits / WaterCellsCount;
    public const float WaterCellSurfaceAreaMeters = MapSurfaceAreaMeters / WaterCellsCount;

    public const float WaterCellVolumeMeters = WaterCellSurfaceAreaMeters * MaxTerrainHeightMeters;

    public static float GetGameUnitsHeightFromMeters(float metersHeight)
    {
      return metersHeight / UnitHeightMeters;
    }

    public static float GetGameUnitsHeightFromWaterCell(float waterCellHeight)
    {
      return (MaxTerrainHeightGameUnits * waterCellHeight) / WaterCellMaxHeightGameUnits;
    }

    public static float GetMetersHeightFromGameUnits(float gameUnitsHeight)
    {
      return gameUnitsHeight * UnitHeightMeters;
    }

    public static float GetMetersHeightFromWaterCell(float waterCellHeight)
    {
      return (MaxTerrainHeightMeters * waterCellHeight) / WaterCellMaxHeightMeters;
    }

    public static float GetWaterCellHeightFromGameUnits(float gameUnitsHeight)
    {
      return gameUnitsHeight * (ushort.MaxValue / MaxTerrainHeightGameUnits);
    }

    public static float GetWaterCellHeightFromMeters(float metersHeight)
    {
      return metersHeight * (ushort.MaxValue / MaxTerrainHeightMeters);
    }

    public static float GetDistanceInMeters(float gameUnitsDistance)
    {
      return (gameUnitsDistance * TileLengthMeters) / TileLengthGameUnits;
    }

    public static float GetDistanceInGameUnits(float metersDistance)
    {
      return (metersDistance * TileLengthGameUnits) / TileLengthMeters;
    }



    /// <summary>
    /// http://hyperphysics.phy-astr.gsu.edu/hbase/Kinetic/relhum.html#c3
    /// http://hyperphysics.phy-astr.gsu.edu/hbase/Kinetic/watvap.html#c1
    /// in g/m3
    /// </summary>
    /// <param name="temp">[C]</param>
    /// <returns></returns>
    public static float GetSaturatedVaporDensity(float temp)
    {
      float tempSquared = temp * temp;
      float tempCubiced = tempSquared * temp;
      return 6.335f + 0.6718f * temp - 0.020887f * tempSquared + 0.00073095f * tempCubiced;
    }

    public static float GetRelativeHumidity(float vaporDensity, float temp)
    {
      return vaporDensity / GetSaturatedVaporDensity(temp);
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="temp">[C]</param>
    /// <param name="relativeHumidity">0-1 range</param>
    /// <returns></returns>
    public static float GetDewPoint(float temp, float relativeHumidity)
    {
      float a = 17.27f;
      float b = 237.7f;

      float core = (a * temp) / (b + temp);
      core = core + (float)Math.Log(relativeHumidity);

      return (b * core) / (a - core);
    }
  }
}
