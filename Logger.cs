﻿using System;
using System.IO;
using System.Reflection;

namespace Runaurufu.Utility
{
  public static class Logger
  {
    // private static readonly string LogFileName = AssemblyConfig.ConfigName + ".log";

    private static string GetLogFileName(Assembly ass)
    {
      return AssemblyConfig.GetConfigName(ass) + ".log";
    }

    public static void Log(string message)
    {
      Assembly callingAssembly = Assembly.GetCallingAssembly();
      File.AppendAllText(GetLogFileName(callingAssembly), message + Environment.NewLine);
    }

    public static void DebugMessage(string message)
    {
      DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, message);
    }

    public static void DebugWarning(string message)
    {
      DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Warning, message);
    }

    public static void DebugError(string message)
    {
      DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Error, message);
    }
  }
}